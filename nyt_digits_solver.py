# Script to solve the New York Times' "Digits" puzzles.
#
# Digits rules, per NYT:
# Combine numbers to reach the target.
#
#     Add, subtract, multiply, and divide any of the six numbers to get as close to the target as you can.
#     You do not have to use all of the numbers.
#     Select a different number to start a new operation.
#     Operations that produce fractions or negative numbers will not be accepted.
#
# Get closer to the target for more stars.
#
#     [3 stars] Reach the target exactly.
#     [2 stars] Reach within 10 of the target.
#     [1 star ] Reach within 25 of the target.
#
# One other wrinkle that I overlooked at first: the result of your previous
# operation does _not_ need to be one of the inputs to your next operation.

import operator

USUAL_SUSPECTS = [operator.add, operator.sub, operator.mul, operator.floordiv]

def op_to_str(op):
    if op == operator.add:
        return "+"
    elif op == operator.sub:
        return "-"
    elif op == operator.mul:
        return "*"
    elif op == operator.floordiv:
        return "/"
    else:
        raise ValueError("Unsupported operator!")


# TODO: numbers should be a list, not a set?
def solve_depth_first(numbers, operators, target, steps):
    if target in numbers:
        return steps

    if len(numbers) < 2:
        return None

    # Obvious optimization: with this structure, we end up needlessly repeating
    # lots of associative operations (e.g. considering both 2+3 and 3+2);
    # eliminate those.
    for n1 in numbers:
        for n2 in numbers - {n1}:
            for op in operators:
                if op == operator.sub and n2 > n1:
                    continue
                if op == operator.floordiv and ((n2 == 0) or (n1 % n2 != 0)):
                    continue

                result = op(n1, n2)
                new_numbers = (numbers - {n1, n2} | {result}).copy()
                history = steps.copy()
                step = f"{n1} {op_to_str(op)} {n2} = {result}"
                history.append(step)

                if result == target:
                    return history
                else:
                    recurse = solve_depth_first(new_numbers, operators, target, history)
                    if recurse is not None:
                        return recurse
    return None

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("numbers", type=int, nargs="+", metavar="number")
    parser.add_argument("target", type=int)

    args = parser.parse_args()
    print("\n".join(solve_depth_first(set(args.numbers), USUAL_SUSPECTS, args.target, [])))
