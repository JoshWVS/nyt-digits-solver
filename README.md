Script to solve the New York Times' [Digits puzzles](https://www.nytimes.com/games/digits).

Provide the input numbers, followed by the target (separated by spaces) as input; see `python nyt_digits_solver.py
--help` for details.
